

install:
	go get .

test: install
	go test ./...

build: install
	go build -o lifx_http .

serve: build
	./lifx_http

clean:
	rm ./lifx_http

pack:
	GOOS=linux make build
	docker build -t lifx_http .

dockserve: pack
	docker run -p 8080:8080 lifx_http lifx_http

upload:
	docker tag lifx_http gcr.io/warm-choir-222420/lifx-http
	docker push gcr.io/warm-choir-222420/lifx-http

ship: test pack upload clean