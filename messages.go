//Copyright Andrew Rogers 2018

package main 

import "encoding/json"


// helper struct to differentiate messages
type InMessage struct {
    MsgType string `json:"msg_type"`
    Msg json.RawMessage `json:"msg"`
}

// helper struct to send individual color data
type ColorMessage struct {
    MsgType string `json:"msg_type"`
    Color string `json:"colorstr"`
}

// sub-message for incoming message that specifies the light selector
// as well as color and/or brightness data
type SelectionMessage struct {
    Selector string `json:"selector"`
    Color string `json:"color"`
    Brightness float64 `json:"brightness"`
}

// sub-message for sending individual light information
type UpdateMessage struct {
    Label string `json:"label"`
    Data interface{} `json:"data"`
}

// format to send messages to javascript
type OutMessage struct {
    MsgType string `json:"msg_type"`
    Msg interface{} `json:"msg"`
}

// helper when receiving account info from Google
type GoogleAccount struct {
    Id string `json:"id"`
    Email string `json:"email"`
    Verified bool `json:"verified_email"`
    Name string `json:"name"`
    FirstName string `'json:"given_name"`
    LastName string `json:"family_name"`
    Link string `json:"link"`
    PictureUrl string `json:"picture"`
}


