# HTTP LIFX Light Controller Web App

**Author: Andrew Rogers**

## License
Copyright © 2018 Andrew Rogers
[This program is licensed under the "MIT License"]
Please see the file LICENSE-MIT in the source
distribution of this software for license terms.


## Design Intent
The goal of this project was to make a toy/tool to manipulate LIFX lights in my home through the use of the LIFX HTTP API.

## Overview of Implementation 

This project is primarily built upon [LIFX's rather robust developer API](https://api.developer.lifx.com/docs/introduction "LIFX Developer Introduction") which is a REST styled API which allows you to:

### API Features
+ Toggle power
+ Query the current state
+ Make a single or multiple aspect state change (in a variety of different ways)
+ Implement some effects
+ Activate pre-made scenes

The project was organized to be built in a Docker container and uploaded to the Google Cloud Kubernetes engine.

The project is basically separated front-end and back-end by which language is being used. The server/back-end is implemented in [Go](https://golang.org/doc/articles/wiki/ "Writing Web Applications in Go") and the front-end is made in javascript with jQuery for assistance. Here is a breakdown of the different files:

### main.go
This is the basic net/http handler controlling the incoming client requests and maintaining the communication with the LIFX API and feeding that information to client to prevent excessive calls directly to the API. The variable **house** maintains the state of the lights. The **commHandler** function is responsible for the Websocket communication with the browser. This also handles the Google OAuth2 authentication. The reference for the authentication is [here](https://dev.to/douglasmakey/oauth2-example-with-go-3n8a)

### lifx.go
This file contains the datatypes that align with the JSON that the LIFX API sends to us. There are also the helper functions to craft and send the GET, PUT, and POST requests to the API. There is a lot of functionality in the structs that are not currently implemented and will allow for more dynamic and modules app development down the road.

### messages.go

This contains the complex data types that the server uses for I/O with the client's javascript code.

### comms.go

This handles both of the server's outbound messaging that are split off as concurrent threads. **PollStatus** acts like a Singleton, maintaining the current state of the **house** variable on a 10 second cycle. **SendUpdate** pushes the current state every two seconds to the Websocket that it's attached to. This is very inefficient and needs to be updated.

### index.html

This is the generic splash page. It's primary function is to provide a link up to the Google login. It uses Bootstrap for its look.

### control.html

This is heart of the front-end, using Bootstrap for the design elements, using cards to organize the different control elements. FontAwesome is used for the graphical icons.

Aside from some javascript to manage converting the color formats, a majority of the jQuery and javascript is built to handle the websocket communication to and from the Go server.

**ws.onmessage** is responsible for the initial setup of the webpage, especially since the number of elements in the Multizone Beam light are determined at setup while the Torchiere light is pre-generated. It is also responsible for the update messages which keep the light colors up to date, regardless of how they are changed.

### What's Not Shown

I have an **auth.go** file where I am keeping the API tokens for Google and LIFX. These would be better handled as environment variables and they are first on my list to be fixed.

## Roadmap of new items to Implement

+ Move API keys and security tokens to environment variables to allow the Docker image to be modular

+ Build all Front End cards dynamically to allow for changes in lights, also allowing the Docker image to be universal. This is important since I just added 6 new lights after I presented the project and now that has added some new wrinkles.

+ Improve the UI, particularly the Color Pallette section to be more intuitive for new users.

+ Improve the status updates

+ Add White (Kelvin Value) Adjustments

## Installation requirements:

This program was developed and tested on Ubuntu 18.04 LTS, but development should be universal once the components are installed.

### Go Language aka Golang

Using Ubuntu for development, I used Ubuntu's golang package, as opposed to the tarball found at [Golang's Webpage](https://golang.org/doc/install). The only reason I used Ubuntu's package was laziness after spending too long sorting out the environment variables **Rust is definitely easier to install and maintain on Linux for some reason**. No matter what, avoid the Snap installer.

For Windows and MacOS, the installation files work fine as I understand it. 

Keep in mind that [The Docker image builds the binary with its own compiler](https://blog.golang.org/docker)

Once installed, you have to type **go get [repository]** to download source code for your local builds. If you are setting things up correctly, they will appear in the ../go/src/[repository] folder.

### Docker

I used the Docker CE or [Community Edition](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository). Since the Google Cloud SDK can be tricky, I think it's the most important that this is installed correctly. Once again, do NOT install from Snap. Here is tutorial specifically for [Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04). I highly recommend the Optional step 2 to allow you to run the docker commands without needing sudo all the time.

### Google Cloud SDK

I really like Google and Google Cloud is fantastic once set up, but [setting it up](https://cloud.google.com/sdk/docs/#deb) if you don't take the time to learn can be quite daunting.

If you are planning to deploy containerized applications, you **must** upload them with the SDK locally. Once it's uploaded, you can handle almost everything else in the UI.

The biggest stumbling block with [Google's Container Registry](https://cloud.google.com/container-registry/docs/advanced-authentication#standalone_docker_credential_helper) compared to Docker Hub is the [authentication setup](https://cloud.google.com/container-registry/docs/advanced-authentication#standalone_docker_credential_helper) that needs to be done since the only way to get an image into a Kubenetes cluster that you've created is through their Container Registry

