// Copyright Andrew Rogers 2018

package main

import (
    "time"
    "log"
    "github.com/gorilla/websocket"
    )

// maintains the status of the house lights as a singleton
func PollStatus(auth string, target *[]Light) {
    
    for {
        time.Sleep(10 * time.Second)
        GetStatus(TOKEN, target)
        log.Printf("Getting Update")
    }
}


// Request a current status of the lights from the API and then send that
// update to the client
func SendUpdate(lights []Light, webSock *websocket.Conn ) {
    
    for {

        time.Sleep(2 * time.Second)

        log.Printf("sending Update")

        for _ , light := range lights {
            if light.Product.Caps.Has_multizone {
                for _, updateZone := range light.LiteZones.Zones {

                    webSock.WriteJSON(OutMessage{"update", UpdateMessage{"zone", updateZone}})

                }
            } else {
                webSock.WriteJSON(OutMessage{"update", UpdateMessage{"bulb", light}})
            }
        }
    }
}