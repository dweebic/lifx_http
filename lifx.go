//copyright andrew rogers 2018


package main

import (
    "net/http"
    "encoding/json"
    "io/ioutil"
    "strings"
    "log"
    
)

type Area struct {
// This handles identifiers for light locations and groupings
// that are user defined within the apps
    Id    string  `json:"id"`
    Name  string  `json:"name"`
}

type ColorVal struct {
// These are the three components that constitute the HSK color
    Hue         float64  `json:"hue"`
    Saturation  float64  `json:"saturation"`
    Kelvin      float64  `json:"kelvin"`
}

type Capabilities struct {
    // This is the set of  physical attributes that define the physical
    // Attributes and limitations of the light
    Has_color  bool  `json:"has_color"`
    Has_var_temp bool `json:"has_variable_color_temp"`
    Min_kelvin  float64 `json:"min_kelvin"`
    Max_kelvin float64  `json:"max_kelvin"`
    Has_ir  bool  `json:"has_ir"`
    Has_multizone bool `json:"has_multizone"`
}

type Product struct {
    // This has the physical attributes as well as the 
    // product naming info set by the manufacturere
    Name    string `json:"name"`
    Company string `json:"company"`
    Identifier  string `json:"identifier"`
    Caps  Capabilities `json:"capabilities"`

}

type Zone struct {
    // Similar to the main light attributes
    // but for the individual elements of a 
    // multizone light element
    Brightness float64 `json:"brightness"`
    Hue float64 `json:"hue"`
    Kelvin float64 `json:"kelvin"`
    Saturation float64 `json:"saturation"`
    Zone float64  `json:"zone"`
}

type Zones struct {
    // For a multizone light, this struct
    // is a container for the individual zones
    Count   float64 `json:"count"`
    Zones   []Zone `json:"zones"`
}

type Light struct {
    // This is the master struct for an individual light 
    Id        string `json:"id"` // light permanent identifier
    Uuid      string  `json:"uuid"` // light account id
    Label     string  `json:"label"` // user adjustable light name
    Connected bool  `json:"connected"` // is the light seen by the API
    Power     string `json:"power"` // power status
    Color     ColorVal `json:"color"` // The color values
    Infrared  string  `json:"infrared"` // The state of the IR emitter if present
    Brightness  float64 `json:"brightness"` // brightness from range 0.0 - 1.0
    LiteZones     Zones `json:"zones"` // Struct of the individual zones if present
    Group       Area `json:"group"` // User adjustable group label
    Location    Area  `json:"location"`// Another user defined grouping label
    Last_Seen   string `json:"last_seen"` // date the light was seen by API
    Seconds  float64  `json:"seconds_since_seen"` // duration since last seen
    Product   Product `json:"product"` // the product identifier
}

type State struct {
    // this respresents the incoming status
    // message
    Power string  `json:"power"` // power state of the light
    Color string  `json:"color"` // color info of the light
    Brightness float64 `json:"brightness"` // light brightness in percentage
    Duration float64 `json:"duration"` // length of the status change animation
    Infrared float64 `json:"infrared"` // If the light has an IR emitter, the status of it
    Fast bool `json:"fast"` // if the transition will be quick
}

type Result struct {
    // the result object format returned after an LIFX request
    Id  string `json:"id"`
    Status string `json:"status"`
    Label string `json:"label"`
}

type Results struct {
    // array of all the results received
    Results []Result `json:"results"`
}

func putRequest(url string, auth string, data string)  {
    // given the url, authentication token and the data payload
    // This function handles the HTTP put requests to make status 
    // changes to the lights
    client := &http.Client{}
    req, err := http.NewRequest(http.MethodPut, url, strings.NewReader(data))
    req.Header.Set("Authorization", auth)
    req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

    if err != nil {
    // handle error
    panic(err)
    }
    resp, err := client.Do(req)
    if err != nil {
    // handle error
    panic(err)
    }
    var requestStatus Results
    resultBody, resultReadErr := ioutil.ReadAll(resp.Body)
    if resultReadErr != nil {
      panic(resultReadErr)
    }
    json.Unmarshal([]byte(resultBody), &requestStatus)
    log.Printf("HTTP: %s\n", resp.Status)
    for _, lightResult := range requestStatus.Results {
    log.Printf("Light: %s -- Status: %s", lightResult.Label, lightResult.Status)
    }

}

func GetStatus(auth string, target *[]Light) {
    // Given the authorization token, returns and array containing all the 
    // structs of the lights in the house

    req, err := http.NewRequest("GET", "https://api.lifx.com/v1/lights/all", nil)
    if err != nil {
      log.Printf("get errer")
    }
    req.Header.Set("Authorization", auth)

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
      // handle err
      log.Printf("error: \n")
    }
    log.Printf("HTTP: %s\n", resp.Status)
    defer resp.Body.Close()
    body, readErr := ioutil.ReadAll(resp.Body)
    if readErr != nil {
      panic(readErr)
    }

    jerr := json.Unmarshal([]byte(body), target)
    if jerr != nil {
        panic(jerr)
    }

    log.Printf("GetStatus -- HTTP: %s\n", resp.Status)


}

func SetState (selector string, auth string, change string) {
    // generates a request to send to the api
    var stateUrl = strings.Join([]string{"https://api.lifx.com/v1/lights", selector, "state"}, "/")
    log.Printf("url: %s\n", stateUrl)

    putRequest(stateUrl, auth, change)


}

func TogglePower( selector string, auth string) {
    // Given the authorization token, toggles the power states of all lights
    // in the house

    var toggleURL = strings.Join([]string{"https://api.lifx.com/v1/lights", selector, "toggle"}, "/")

    req, err := http.NewRequest("POST", toggleURL, nil)
    if err != nil {
      log.Printf("get errer")
    }
    req.Header.Set("Authorization", auth)

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
      // handle err
      log.Printf("error: \n")
    }

    var requestStatus Results
    resultBody, resultReadErr := ioutil.ReadAll(resp.Body)
    if resultReadErr != nil {
      panic(resultReadErr)
    }

    json.Unmarshal([]byte(resultBody), &requestStatus)
    for _, lightResult := range requestStatus.Results {
    log.Printf("Light: %s -- Status: %s", lightResult.Label, lightResult.Status)
    }

    log.Printf("HTTP: %s\n", resp.Status)
    defer resp.Body.Close()
}

