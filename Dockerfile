# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Copy the local package files to the container's workspace.
ADD . /go/src/bitbucket.org/dweebic/lifx_http


RUN mkdir /app 
ADD . /app/
WORKDIR /app 

# Build the lifx_http command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go get github.com/gorilla/websocket
RUN go get -u cloud.google.com/go/compute/metadata
RUN go get golang.org/x/oauth2
RUN go build -o lifx_http .

# Run the lifx_http command by default when the container starts.
ENTRYPOINT /app/lifx_http
