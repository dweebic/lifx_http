// Copyright Andrew Rogers 2018

// This is the main server file. This is a http webserver serving static
// webpages as well as a websocket connection once the client has completed
// the Google authentication process.

package main

import (
    "flag"
    "log"
    "net/http"
    "fmt"
    "io/ioutil"
    "encoding/json"
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/google"
    "github.com/gorilla/websocket"
    "strings"

)

var (
    googleOauthConfig *oauth2.Config
    // TODO: randomize it
    oauthStateString = "pseudo-random"
    house []Light
)

var addr = flag.String("addr", "0.0.0.0:8080", "http service address")
var upgrader = websocket.Upgrader{} // use default options

func init() {
    // using examples from https://dev.to/douglasmakey/oauth2-example-with-go-3n8a
    googleOauthConfig = &oauth2.Config{
        RedirectURL:  "http://127.0.0.1:8080/home", //TODO: make external environment variable
        ClientID:     CID, //TODO: make external environment variable
        ClientSecret: CSECRET, //TODO: make external environment variable
        Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
        Endpoint:     google.Endpoint,
    }
}


// handler for working with the Google Auth API
// based on https://dev.to/douglasmakey/oauth2-example-with-go-3n8a
func getUserInfo(state string, code string) (string, error) {
    if state != oauthStateString {
        return "nil", fmt.Errorf("invalid oauth state, received %s for state", state)
    }

    token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
    if err != nil {
        return "nil", fmt.Errorf("code exchange failed: %s", err.Error())
    }

    response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
    if err != nil {
        return "nil", fmt.Errorf("failed getting user info: %s", err.Error())
    }
    test , err := ioutil.ReadAll(response.Body)
    defer response.Body.Close()

    if err != nil {
        return "nil", fmt.Errorf("failed reading response body: %s", err.Error())
    }

    var userAccount GoogleAccount
    json.Unmarshal(test, &userAccount)

    if ((strings.TrimSpace(userAccount.Email) != "user@email.com") && (strings.TrimSpace(userAccount.Email) != "user@email.com")) {
        return "nil", fmt.Errorf("UnAuthorized Account: %s", userAccount.Email)
    }

    return "passed", nil
}


// This is the handler to serve the control webpage after receiving the callback
// from the Google Sign-in
func home(writer http.ResponseWriter, req *http.Request) {

    checkPoint, err := getUserInfo(req.FormValue("state"),req.FormValue("code"))
    if err != nil {
        fmt.Println(err.Error())
        http.Redirect(writer, req, "/", http.StatusTemporaryRedirect)
        return
    }
    log.Printf("checkPoint: %v", checkPoint)
    http.ServeFile(writer, req, "control.html")

}

// This is the handler for the websocket communication
func commHandler (writer http.ResponseWriter, req *http.Request) {
    
    upgrader.CheckOrigin = func(req *http.Request) bool { return true }
    conn, err := upgrader.Upgrade(writer, req, nil)
    if err != nil {
        log.Print("upgrade:", err)
        return
    }

    zoneMsg := OutMessage{"setup", house}

    conn.WriteJSON(zoneMsg)
    go SendUpdate(house, conn)


    defer conn.Close()


    for { // continuous message handling loop

        msg := InMessage{}
        conn.ReadJSON(&msg)
        log.Printf("recv: %v", msg.MsgType)

        var selector SelectionMessage
        json.Unmarshal(msg.Msg, &selector)
        log.Printf("selector: %v", selector)

        switch msg.MsgType {

            case "color": // arguments to update the color of the lights
                log.Printf("color")
                colorChange := fmt.Sprintf(`power=on&color=%s`, selector.Color)
                SetState(selector.Selector, TOKEN, colorChange)

            case "toggle": // list of lights to toggle power 
                log.Printf("TogglePower")
                TogglePower(selector.Selector, TOKEN)

            case "brightness": 
                log.Printf("brightness")
                brightChange := fmt.Sprintf(`power=on&brightness=%v`, selector.Brightness)
                log.Printf("brightChange = %s", brightChange)
                SetState(selector.Selector, TOKEN, brightChange)


            default:
                log.Printf("Unhandled message: %v", msg.Msg)
        }
    }
}

func handleGoogleLogin(writer http.ResponseWriter, req *http.Request) {
    // serves the login request to the Google OAuth API
    log.Printf("Google Login request")
    url := googleOauthConfig.AuthCodeURL(oauthStateString)
    http.Redirect(writer, req, url, http.StatusTemporaryRedirect)
}

func index (writer http.ResponseWriter, req *http.Request) {
    // The basic splash static index handler
    log.Printf("Serving Index")
    http.ServeFile(writer, req, "index.html")
    
}

func main() {

    log.Printf("Serving on port 8080....")

    // maintain a single instance concurrently to update the status of the lights
    go PollStatus(TOKEN, &house) 

    // The route/page request handlers
    flag.Parse()
    log.SetFlags(0)
    http.HandleFunc("/", index)
    http.HandleFunc("/login", handleGoogleLogin)
    http.HandleFunc("/home", home)
    http.HandleFunc("/comms", commHandler)
    log.Fatal(http.ListenAndServe(*addr, nil))
}